from pathlib import Path
import json, random, os, glob, io, re, logging
import whisper

logging.basicConfig(level=logging.INFO)

logging.info("Loading list of media")
videos = list(Path('./downloads').glob('*.mp*'))

logging.info("Loading list of already processed media")
text_ids  = [p.name.replace(".json", "") for p in list(Path('./downloads').glob('*.json'))]

logging.info("Loading whisper model")
model = whisper.load_model("medium")

# goes through all media without transcription and uses openai's whisper
# model to get a transcription and store it in JSON file
for video_file in videos:
    id = re.sub(r'\.mp\d', '', video_file.name)
    if id in text_ids:
        continue
    logger.info(f"Transcribing {id}")
    try:
        result = model.transcribe(str(video_file))
        with open(f"downloads/{id}.json", "w") as file:
            file.write(json.dumps(result))
    except Exception:
        logging.error(f"error in {id}")

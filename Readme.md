# Experiment: Tiktok Visualisierung 

## Technisches

Der Code für dieses Projekt ist explorativ im Umgang mit TikTok als mir fremdes System entstanden und "nebenbei" über mehrere Tage gewachsen.

### Scraping der Metadaten

Erster Schritt war das Scrapen von Metadaten von TikTok mittels des [TikTokPy](https://tiktokpy.readthedocs.io/en/latest) Packages. Diese Software nutzt intern einen headless Browser, jeder Call startet also eine neue Chrome- oder Firefox Instanz als Sub-Prozess. Das Script `scrape.py` akzeptiert einen Usernamen oder Challenge (TikTok-Lingo für Hashtag), lädt Metadaten für alle sichtbaren Videos und speichert diese in einzelnen JSON Dateien im `videos` Ordner (nicht Teil dieses Repositories).

Dieser Prozess lässt sich sehr einfach händisch seeden.

Anschließend lassen sich trivial Listen von Usern und Challenges generieren. Dieser Prozess lässt sich im Laufe des Scrapings beliebig wiederholen.

    cat videos/*.json | jq -r .author | sort | uniq > users.txt
    cat videos/*.json | jq -r ".challenges[] | .title" | sort | uniq > challenges.txt

`challenges.txt` wurde manuell bearbeitet um grob Challenges in Alphabeten die nicht Lateinisch oder Arabisch im Ursprung sind zu entfernen und die Datensammlung wenigstens ein wenig zu fokussieren. 

Eine ernsthafte Implementierung würde natürlich an dieser Stelle einen Scheduler mit Priorisierung und Fortschritts-Tracking nutzen. Um blind ein kleines Sample zu ziehen lohnt der Aufwand allerdings nicht, die Liste an Challenges und Users steigt explosionsartig in sechsstellige Größen, während das Scraping mit zwei parallelen Prozessen nur 15,000 - 20,000 Video Metadaten am Tag erreicht hat.

Daher wurde also blind einmal User und einmal Challenges gescraped um wenigstens einen Bias durch frühe Samples zu vermeiden:

    while true ; do python scrape.py -c `shuf -n 1 challenges.txt` ; done
    while true ; do python scrape.py -u `shuf -n 1 users.txt` ; done

Eine Visualiserung eines Graphen, der User mittels Überschneidungen verwendeter Challenges verbindet und clustert wurde implementiert, durch die willkürliche Verwendung von Challenges wurde der Ansatz jedoch verworfen.

Das Scraping wurde nach einigen Tagen beendet, nachdem gut 50,000 Metadaten gesammelt wurden.

### Download von Videos / Tonspuren

Nachdem sich herausgestellt hat, daß weitere Daten hilfreich wären und kleinen Experimenten mit AI Modellen zum Transkribieren von Audio in diversen Sprachen, fiel die Entscheidung, die gesprochenen Texte aus Videos zu extrahieren.

Die bereits gesammelten Metadaten enthalten auch Download-Links für Videos und Tonspuren, leider schützt TikTok den Videodownload vor direktem Scraping mit Browser-basierten Validierungen. Es ist also erforderlich, mit einem headless Browser das Video aufzurufen. Da der eigentliche Download im Browser problematisch ist, empfiehlt es sich, die von TikTok gesetzten Cookies zu nutzen um die Dateien mit einem Script zu laden.

In einer zukünftigen Implementierung wäre es natürlich sinnvoll, den Download bereits beim Scraping zu erledigen, um die Seiten nicht ein zweites mal laden zu müssen.

`python download.py` lädt alle existierenden Metadaten im `videos` Verzeichnis und lädt die dort angegebene Tonspur als MP3 herunter, sofern sie noch nicht vorhanden ist und speichert sie im `downloads` Verzeichnis. Es hat sich gezeigt, daß diese Tonspur nicht unbedingt immer der echten Tonspur der Videos entspricht, insbesondere, wenn Hintergrundmusik von TikTok identifiziert und extrahiert wurde. Aus Platz- und Zeitgründen wurde darauf verzichtet, Videodaten zu laden. Statt dessen wurden bei der Auswertung diejenigen Videos, in denen ein Albumtitel in den Metadaten angegeben ist, ausgeschlossen.


### Transkription

`python video_to_text.py` liest alle .mp3/4 Dateien und bereits bestehende Transkriptionen im `downloads` Verzeichnis ein, extrahiert gesprochenen Text und wahrscheinlichste Sprache aus Video- und Tondateien und speichert diese als JSON Datei ebenfalls im `downloads` Verzeichnis. Für 50,000 Tonspuren mit etwa 47GB hat dies auf einer GPU etwa 5 Tage gedauert.


### Analyse

Parallel zum Download und der Text-Extraktion konnte damit begonnen werden, die Daten interaktiv in einem Jupyter Notebook zu betrachten und anzureichern. Dieses ist in TikTokExploration.ipynb dokumentiert.


## Zukünftiges

Der hier verwendete, Datei-basierte Ansatz funktioniert sehr gut in einer explorativen Phase eines Projektes durch grenzenlose Flexibilität. Im produktiven Einsatz hingegen gelten andere Trade-Offs. Ausgehend von dem hier existierenden, finalen Datensatz aus Video-Metadaten, extrahiertem Text und Sprache und Embeddings wäre folgende Architektur denkbar:

### Scraper

Aktuell werden die genannten Phasen in 4 unterschiedlichen Prozessen geladen beziehungsweise berechnet. In einer neuen Implementierung würde ein Scraper eine Video-Id mit Kontext von einem Scheduler erhalten und anhand derer die Metadaten laden, das Video herunterladen, Text und Sprache extrahieren, Embeddings berechnen und alles zusammen in einem einheitlichen, validierten Format sichern.
Ansatzpunkte für weitere Scrapes, etwa durch Empfehlungen ähnlicher Videos, kann der Scraper dem Scheduler mit dem notwendigen Kontext zur Priorisierung und Datenanreicherung zusammen mit einer Erfolgs- oder Fehlermeldung zurückgeben.

### Scheduler

Dieser Service kann beispielsweise als API oder Redis-Datenbank zur Koordination beliebig vieler Scraper dienen. Er führt eine interne Liste alle bekannten Ressourcen und deren Status und Priorität. Dabei erhält er von Scrapern Hinweise zu neuen Ressourcen, kann aber auch auf anderem Wege gefüttert werden. So ist etwa ein Chatbot denkbar, dem man einen Link zu einem interessanten User oder Video schickt und dank hoher Priorität sind deren Daten Minuten später auswertbar in der Datenbank.

### Monitoring

TikTok Scraping extrahiert Daten aus dynamischen Webseiten, die sie ständig ändern oder Ratelimits unterzogen werden können. Für eine kontinuierliche Überwachung von kritischen Kanälen muss daher sichergestellt sein, daß die erforderliche Bandbreite und Vollständigkeit eingehalten und Ausfälle schnell detektiert, alarmiert und kompensiert werden können. 

### Rohdaten zu Datensätzen Pipelines

Für sehr viele Anwendungsfälle sind simple, strukturierte Dateien wie JSON oder CSV unschlagbar in Performance und Flexibilität. Daher empfiehlt es sich eigentlich immer, anfallende Rohdaten als Zwischenprodukt in der Kette zwischen Datenerhebung und Speicherung in Anwendungs-spezifische Systeme beizubehalten. Dieses Vorgehen erlaubt auch ein schnelleres, sichereres Entwickeln: Statt sich anfangs festlegen zu müssen, welche Daten man behält oder verwirft, lassen sich im Hintergrund von Anfang an Daten sammeln während man am anderen Ende schlanke, auf spezifische Anwendungen optimierte Datenbanken aufsetzen kann, die sich bei Bedarf in Minuten wegwerfen und neu füllen lassen.

### Bigger Picture

Will man Trends, Ereignisse, Personen über mehrere Plattformen hinweg einheitlich betrachten können, müssen auch Datenstrukturen einheitlich gedacht werden. Abhängig von Anforderungen und inhaltlichen wie technischen Grenzen müssen Elemente nach identischen oder wenigstens klar benennbaren Schemata identifiziert oder klassifizert werden um dann über die Grenzen einzelner Datensilo hinaus durchsuchbar und einheitlich aggregierbar zu werden.

Eventuell erfordert dies mittelfristig neben den eigentlichen Datenbanken auch eine "Entity Database". Diese kann dabei helfen, Klarnamen zu Profilnamen verschiedener Plattformen zuzuordnen, Beschreibungen und Trainingsdaten zu in AI Modellen verwendeten Kategorien zu dokumentieren, also generell parallele Suchanfragen in unterschiedliche Datensätze sinnvoll anzureichern.
Beispiel: "Wie finde ich Ibrahims Accounts bei TikTok, Telegram, Twitter...?" oder "In der TikTok Datenbank finde ich Accounts mit `is_antisemitism` und bei Telegram finde ich Kanäle mit `channel_antisemitism`, ist das vergleichbar und wie finde ich einen Trend für antisemitische Posts über alle Plattformen hinweg?"

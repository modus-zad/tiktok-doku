# https://tiktokpy.readthedocs.io/en/latest
from tiktokapipy.api import TikTokAPI
import argparse, logging, sys

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--challenge", type=str)
parser.add_argument("-u", "--user", type=str)
args = parser.parse_args()
logging.basicConfig(level=logging.INFO)

# downloads all videos visible in a response for a single search by user or challenge
# (hashtag) as given by calling arguments.
# call this script like
#
#   while true ; do python scrape.py -c `shuf -n 1 challenges.txt` ; done
# or
#   while read user; do python scrape.py -u $user ; done < users.txt
#
# to use a simple random or ordered scraping strategy, where challenges.txt/users.txt are
# plain text files with one challenge/user per line.
# realworld-usage would require a scheduler to prioritize and track targets while optimizing
# resource usage and respect rate-limits of scraped endpoints.

if __name__ == '__main__':
    with TikTokAPI(navigation_retries=2, navigation_timeout=10) as api:
        # make query to selected endpoint
        if args.challenge:
            logging.info(f"fetching results for challenge {args.challenge}")
            response = api.challenge(args.challenge, scroll_down_time=20)
        elif args.user:
            logging.info(f"fetching results for user {args.user}")
            response = api.user(args.user, scroll_down_time=20)
        else:
            logging.error(f"missing parameter for -u for user or -c for challenge to scrape")
            sys.exit()

        logging.info(f"  done, fetching videos...")

        # get videos, store response
        # TODO: add scraping metadata (request parameters, time, proxy ip/country etc.)
        # TODO: download mp4/mp3 and extract text at this stage. This avoids
        #       extra queues for those jobs and stitching the datasets together
        #       for analysis. requires minor rewrites but
        for video in response.videos.sorted_by(lambda vid: vid.create_time):
            with open(f"videos/{video.id}.json", "w") as file:
                file.write(video.json(exclude={ "creator", "tags", "comments" }))
        logging.info(f"  done, exiting")

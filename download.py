from tiktokapipy.async_api import AsyncTikTokAPI
from tiktokapipy.models.video import Video
import asyncio
import requests
import os, glob, io, json, random, argparse, logging
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--id", type=str)
args = parser.parse_args()
logging.basicConfig(level=logging.INFO)

def video_link(video_id: int) -> str:
    return f"https://m.tiktok.com/v/{video_id}"

async def download(url, filename, cookies):
    response = requests.get(
        url,
        cookies = cookies,
        headers = { 'referer': 'https://www.tiktok.com/' }
    )
    with open(filename, mode='wb') as f:
        f.write(response.content)
        f.close()


async def get_audio(video: Video, cookies):
    headers = { 'referer': 'https://www.tiktok.com/' }
    response = requests.get(
        video.music.play_url,
        cookies=cookies,
        headers=headers,
    )
    with open(f"downloads/{video.id}.mp3", mode='wb') as f:
        f.write(response.content)
        f.close()


async def get_video(video: Video, cookies):
    headers = { 'referer': 'https://www.tiktok.com/' }
    response = requests.get(
        video.video.download_addr,
        cookies=cookies,
        headers=headers,
    )
    with open(f"downloads/{video.id}.mp4", mode='wb') as f:
        f.write(response.content)
        f.close()


async def run(id):
    async with AsyncTikTokAPI(emulate_mobile=False) as api:
        url = video_link(id)
        logging.info(f"Navigating to {url}")
        video: Video = await api.video(url)

        # map cookies from headless browser for actual file download
        cookies = {cookie['name']: cookie['value'] for cookie in await api.context.cookies()}

        # only download mp3 because of time/space constraints.
        # better: if video.image_post or video.audio.album.is not None, then immediately extract text
        # from audio and unlink file
        if True:
            url = video.music.play_url
            filename = f"downloads/{video.id}.mp3"
        else:
            url = video.video.download_addr
            filename = f"downloads/{video.id}.mp4"

        logging.info(f"  Download {url} to {filename}")
        await download(url, filename, cookies)
        logging.info(f"  Done")

# get all video metadata and download mp4/mp3 where not present in download folde
async def scrape_all():
    files = list(Path('./videos').glob('*.json'))
    random.shuffle(files)
    for p in files:
        with open(p, 'r') as json_file:
            video = json.load(json_file)
            if(os.path.isfile(f"downloads/{video['id']}.mp3") or os.path.isfile(f"downloads/{video['id']}.mp4")):
                continue
            try:
                await run(video['id'])
            except Exception:
                logger.error(f"Error in {p}, ignoring")


if __name__ == '__main__':
    # download single video
    # asyncio.run(run(args.id))

    # download all
    asyncio.run(scrape_all())

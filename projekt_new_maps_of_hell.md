# Projekt "New Maps of Hell"

Ein Framework, um Daten aus unterschiedlichen Quellen, mit diversen
Methoden gewonnen, einheitlich zu organisieren, entlang nützlicher
Dimensionen zu analysieren und visualisieren und für interne und externe
Stakeholder aufzubereiten.

## Problembeschreibung

Der zu untersuchende Problembereich erstreckt sich über mehrere
Dimensionen, die miteinander interagieren und nicht getrennt voneinander
betrachtet werden können.

Einige davon sind:

### Phänomenbereiche

Rechts- und Linksextremismus, religiös oder säkular-apokalyptisch motiviert
gibt es zahlreiche Gruppierungen und Trends die gezielt oder vergleichend
zu untersuchen sind.

### Plattformen

Telegram, TikTok, Twitter, Videospiele uvm. manifestieren
unterschiedliche Ausdrücke und stellen jeweils eigene Anforderungen und
Einschränkungen an Erhebung, Auswertbarkeit, Vollständigkeit.

### Akteure

Individuen und Organisationen sind mit mehr als einer Person und
Botschaft auf mehr als einer Plattformen vertreten.

### Aktuelle Gesellschaftliche Ereignisse, Personen und Trends

Heizungshammer, Flüchtlingswelle, Ukrainekrieg, Lauterbach, Genderstern
dienen als Kristallisationskeim für politischen Diskurs und Angriffe und
lassen sich als Meta-Themen aggregieren.

### Narrative, Stilmittel, Angriffsformen

Inhaltlich setzen sich Botschaften aus weiteren Dimensionen zusammen:
Antisemitisch geprägte Interpretationen aktueller Ereignisse,
Boykottaufrufe gegen "woke" Firmen, Mobilisierung zu Orten und Terminen,
Todesdrohungen gegen Politiker*innen.


Dem gegenüber existieren und entstehen Datensilos, die technisch und
personell limitiert einzelne Aspekte des Phänomenbereichs erfassen:

### Scraping

Ein automatisiertes Scraping von ausgewählten Kanälen bestimmter
Plattformen liefert täglich zehntausend neue Datensätze, die im Vorfeld
unbekannte Fragen beantworten sollen.

### Qualitative Analysen

Expertenanalysen, händisch gesammelte Informationen und Studienergebnisse
sind wertvolle Assets, die über einen ursprünglichen Zweck hinaus Verwendung
finden sollten.

### Externe Daten

Ein Austausch von Daten mit Dritten ist denkbar, um Expertise und
Ressourcen optimal einzusetzen.


## Vision

In einer perfekten Welt sind alle oben genannten Dimensionen
definierbar und automatisiert qualifizierbar.
Alle verfügbaren Datenquellen fließen in einem einheitlichen Format
zusammen, gegebenenfalls zusätzlich zu Transformationen für andere
Auswertungen.
Jede neu gelesene Nachricht auf Telegram, jeder Tweet, ist entlang
dieser Dimensionen vollständig klassifiziert und fließt in das Gesamtbild ein.

Jede dieser Dimensionen kann visualisiert werden, etwa als
semi-statischer Graph, der Zusammenhänge optisch darstellt und langsam
über die Zeit wächst.

Unter diesen Vorraussetzungen wäre es ein Leichtes, zu sehen, wie sich
etwa ein neues Narrativ über verschiedene Plattformen, zwischen Akteuren
uvm verbreitet, welche Kanäle es in welchem Ton aufgreifen, wofür
das rechte Spektrum mobilisiert und wie das Linke darauf reagiert und ob
die Namen von Salafisten in Chats von Call of Duty erscheinen.

Zu Evergreen- und neuen Themen lassen sich schnell Erkenntnisse
gewinnen und für bestehende und neue Stakeholder visualisieren, im
Netzwerk, als Trendlinien oder tabellarisch.
Fehlende Datenquellen lassen sich einfach finden und an den aktuellen
Bedürfnissen priorisieren und und das Gesamtbild integrieren.

Offensichtlich kann dieses Gesamtbild niemals vollständig sein. Bias
bei der Quellenwahl und Kategoriesierung sind unvermeidbar, die
Landschaft, verwendeten Codes und impliziten Bedeutungen verändern sich
ständig und können niemals fehlerfrei klassifiziert werden und sind
nicht immer direkt vergleichbar.

Dennoch kann ein gemeinsames Format die Basis für neue Wege der
Auswertung und schnellere, umfassendere Generierung von Value für
externe und interne Stakeholder bieten.

Nicht zuletzt erfordert dieser Ansatz Überlegungen, Expertise und
Infrastruktur, die Synergien entlang des gesamten Spektrums an
Datenquellen schafft:


## Technische Prinzipien und Roadmap


## Datenstruktur

Kern dieses Systems ist eine einfache Tabelle, die beispielsweise in
Postgres, aber auch praktisch jeder anderen Datenbank persistiert werden
kann, in der jede Zeile einen Datenpunkt speichert, der Quellplattform,
Author-ID, Zeitpunkt und Labels für alle erkannten Dimensionen
beinhaltet:

`ID;SOURCE_ID;SOURCE_TYPE;SOURCE_CHANNEL;....;LABELS`
`..;.........;TELEGRAM;@qanon123;....;[AUTHOR:unidentified,LOCATION:hamburg,HATE:antisemitism>low,HATE:deaththreat>high,TYPE:Conspiracy>QAnon,NARRATIV:great_reset,PERSON:karl_lauterbach,ORGANISATION:who,...]`
`..;.........;TIKTOK;@islamcontent7654;....;[AUTHOR:ibrahim_xxx,HATE:antisemitism>mid,TYPE:Religious>Islam>Salafist,PERSON:olaf_scholz,PERSON:robert_habeck,...]`

Die Auswertung und Visualisierung erfordert nur sehr einfache SQL
Queries, die Dashboards und Exporte in diverse Formate wie .gefx, Excel,
CSV füttern können oder in Python direkt als Pandas Dataframe, DuckDB
oder ähnliches genutzt werden kann.

Die große Herausforderung und Chance liegt auf dem Weg in diese
Datenbank.

## Database of Evil

Mit steigender Zahl an zu untersuchenden Dimensionen steigt die Zahl der
verwendeten Labels und das benötigte Wissen, diese zu Erzeugen und zu
Interpretieren.
Daher braucht es eine Datenbank, die die fachliche Expertise für Mensch
und Maschine dokumentiert, wie Labels erzeugt und interpretiert werden
müssen.

Beispiele dazu:

Die Person "Ibrahim X" gehört zum Typ `TYPE:Religious>Islam>Salafist`
und ist damit entlang dieser Dimension unter `Religious`,
`Religious>Islam` und `Religious>Islam>Salafist` mit allen Äußerungen
gelabelt und segmentierbar.
Ferner ist eine Liste aller bekannter Accounts zu hinterlegen, die
automatisiert erlaubt, das `AUTHOR:ibrahim_xxx` Label bei allen seinen
Äußerungen zu setzen, aber auch ein `PERSON:ibrahim_xxx` Label bei allen
Äußerungen anderer Quellen, die einen seiner bekannten Accounts
referenzieren.

Für Fälle, die nicht mit hart codierten Regeln zu lösen sind, müssen
ML/AI Modelle die Arbeit übernehmen. Diese benötigen in der Regel
Trainingsdaten, die in dieser Datenbank gepflegt werden können.
Diese Datensätze können helfen, Todesdrohungen und andere Hassrede zu
identifizieren, aber auch projektspezifische Entitäten und Themen, etwa
politische Parteien, aktuelle Narrative und Sentiments.

Der Aufbau, die Strukturierung und Pflege einer derartigen Datenbank ist
ein großes Investment, das dafür aber auch direkt in alle
Arbeitsbereiche einzahlt, als Knowledge Base, als Übersicht über das
eigene Datenportfolio, als Anknüpfungspunkt für Austausch mit externen
Akteuren und als Trainingsdatensatz, der als eigenständiges
Arbeitsergebnis Dritten zugängig gemacht werden kann.


## Ein realistischer Pfad

Ein derartiges System auf der "grünen Wiese" zu planen und zu
implementieren ist möglich, aber unrealistisch.
In der realen Welt müssen Ressourcen entsprechend der Möglichkeiten
und Prioritäten in einem ständigen Proess angepasst werden.


